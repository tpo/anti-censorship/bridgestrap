module gitlab.torproject.org/tpo/anti-censorship/bridgestrap

go 1.21

toolchain go1.22.1

require (
	github.com/gorilla/mux v1.8.1
	github.com/prometheus/client_golang v1.19.0
	github.com/yawning/bulb v0.0.0-20170405033506-85d80d893c3d
	golang.org/x/time v0.5.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/common v0.53.0 // indirect
	github.com/prometheus/procfs v0.14.0 // indirect
	golang.org/x/net v0.22.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	google.golang.org/protobuf v1.34.1 // indirect
)
